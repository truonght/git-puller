<?php

define('MAIN_PATH', '../');
define('SLACK_TOKEN', 'xoxp-9331324741-10277271923-17830174855-cf1dc35719');
define('SLACK_CHANEL', '#git-puller');
define('SLACK_USERNAME', 'monoboot');

// ...
if (empty($_REQUEST['r'])) {
    echo 'Invalid repository name.';
    exit;
}

// Autoload
require 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\SlackHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\JsonFormatter;
use Monolog\Formatter\LineFormatter;
use Monolog\Formatter\GelfMessageFormatter;
use Tmd\AutoGitPull\Deployer;

/**
 * //
 *
 * @param  string $variable
 * @return string
 */
function clean_request($variable)
{
    return str_replace(['/', '\\'], '', htmlentities($variable));
}

// Get paths.
$repo = clean_request($_REQUEST['r']);
$root_directory = realpath(MAIN_PATH).'/'.$repo;
$directory = $root_directory;

// Get branch
$branch = empty($_REQUEST['b']) ? 'master' : $_REQUEST['b'];

// If we have wordpress theme
$theme = empty($_REQUEST['t']) ? $repo : clean_request($_REQUEST['t']);
if (is_dir($theme_path = $root_directory.'/wp-content/themes/'.$theme)) {
    $directory = $theme_path;
}

// If we have wordpress plugin
if (! empty($_REQUEST['p'])) {
    $plugin = clean_request($_REQUEST['p']);
    $directory = $root_directory.'/wp-content/plugins/'.$plugin;
}

// Logger
$logger = new Logger($repo);
$slackLogger = clone $logger;

$slackHandler  = new SlackHandler(SLACK_TOKEN, SLACK_CHANEL, SLACK_USERNAME, true, 'robot_face', Logger::DEBUG, true, false, true);
$streamHandler = new StreamHandler('php://output');

$slackHandler->setFormatter(new LineFormatter("[%datetime%] %channel%: %message%\n"));
$streamHandler->setFormatter(new LineFormatter("[%datetime%] %level_name%: %message% %context%\n"));

$logger->pushHandler($streamHandler);
$slackLogger->pushHandler($slackHandler);

// Start deployer
ob_start();

// Deployer
$deployer = new Deployer([
    'branch' => $branch,
    'directory' => $directory,
]);

$deployer->setLogger($logger);

try {
    $deployer->deploy();

    $slackLogMessage = 'Deploy successfully!';
    $slackLogLevel   = Logger::INFO;
} catch (Exception $e) {
    $slackLogMessage = 'Deploy failed!';
    $slackLogLevel   = Logger::ERROR;
}

$context = ob_get_contents();
$context = explode("\n", $context);

$buildContext = [];
foreach ($context as $key => $value) {
    if (preg_match('/^\[(.*)\]\s(.*)/', trim($value), $matches)) {
        $id = '#'.($key+1).' - '.$matches[1];
        $buildContext[$id] = str_replace('[]', '', $matches[2]);
    }
}

$slackLogger->log($slackLogLevel, $slackLogMessage, $buildContext);
exit;
